export const AirportsService = ['$http','$q', ($http, $q) => {

  const url ='https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/';
  let airports = [];
  let routes = {};
  let data = {};

  let setData = (loadeData) => {
    data = loadeData;
    airports = loadeData.airports;
    routes = loadeData.routes;
  }

  let loadFromApi = () => {
    return $http.get(url).then((response) => {
        setData(response.data);
        return airports;
      }).catch(() => airports);
  }

  return {
    find: () => {
      return $q(function(resolve) {
          if(airports.length == 0) {
            resolve(loadFromApi());
          } else {
            resolve(airports);
          }
      });
    },
    findRoutes: (code) => {
      return $q(function(resolve) {
        let routeCodes = routes[code];
        let destinations = [];
        routeCodes.forEach(function(code) {
          destinations.push(airports.find(function(airport){
            return airport.iataCode == code;
          }));
        });
        resolve(destinations);
      });
    }
  }

}];

export default AirportsService;
