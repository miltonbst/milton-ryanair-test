import template from './ryanair-autocomplete.html';
import controller from './ryanair-autocomplete.controller';
import './ryanair-autocomplete.scss';

export const RyanAirAutocompleteComponent = {
  bindings: {
    items: '<',
		onSelect:'&'
  },
  template,
  controller
};

export default RyanAirAutocompleteComponent;
