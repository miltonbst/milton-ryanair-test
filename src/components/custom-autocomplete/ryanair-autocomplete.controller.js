export default function AirportSelectorController($scope, $timeout, $element) {
  'ngInject';

  let ctrl = this;

  ctrl.current=0;
  ctrl.selected=true;

  ctrl.focus = function () {
    ctrl.selected = false;
  }

  ctrl.blur = function () {
    $timeout(function(){
      ctrl.selected = true;
    }, 160);
  }

  ctrl.handleSelection = (selectedItem) => {
    ctrl.selectedItem = selectedItem;
    ctrl.model=selectedItem.name;
    ctrl.current=0;
    ctrl.selected=true;
    $timeout(function(){
      ctrl.onSelect({'selected': ctrl.selectedItem});
    }, 150);
  };

  ctrl.isCurrent=function(index){
    return ctrl.current==index;
  };

  ctrl.setCurrent=function(index){
    ctrl.current=index;
  };

}
