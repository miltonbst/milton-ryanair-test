import template from './date-selector.component.html';
import controller from './date-selector.controller';
import './date-selector.scss';

export const DateSelectorComponent = {
  bindings: {
    date: '<',
    onDateSelected: '&'
  },
  template,
  controller
};
