import moment from 'moment';

export default function SearchWrapperController($scope, $state, AirportsService) {
  'ngInject';

  let ctrl = this;
  ctrl.originAirports = [];
  ctrl.destinationAirports = [];
  ctrl.origin = {};
  ctrl.destination = {};

  ctrl.getAirports = () => AirportsService.find().then((airports) => {
     ctrl.originAirports = airports;
  });

  ctrl.onOriginSelected = (selectedAirport) => {
    ctrl.origin = selectedAirport;
    AirportsService.findRoutes(selectedAirport.iataCode).then((destinations) => {
      ctrl.destinationAirports = destinations;
    });
  }

  ctrl.onDestinationSelected = (selectedAirport) => {
    ctrl.destination = selectedAirport;
  }

  ctrl.setStartDate = (selected) => {
    ctrl.startDate = selected;
  }

  ctrl.setEndDate = (selected) => {
    ctrl.endDate = selected;
  }

  this.find = () => {
    $scope.error = false;

    if (typeof ctrl.origin === 'undefined' || typeof ctrl.destination === 'undefined'
      || typeof ctrl.startDate === 'undefined' || typeof ctrl.endDate === 'undefined') {
      $scope.error = true;
      return;
    }

    const origin = ctrl.origin.iataCode;
    const destination = ctrl.destination.iataCode;
    const start = moment(ctrl.startDate).format('YYYY-MM-DD');
    const end = moment(ctrl.endDate).format('YYYY-MM-DD');

    $state.go('home.list', { origin, destination, start, end });
  };

  this.$onInit = () => {
    ctrl.getAirports();
  }

}
