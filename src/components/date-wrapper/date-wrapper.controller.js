import moment from 'moment';

export default function DateWrapperController($scope) {
  'ngInject';

  let ctrl = this;

  ctrl.startDateSelected = (selected) => {
    ctrl.startDate = selected;
    if (moment(ctrl.startDate) > moment(ctrl.endDate)) {
      ctrl.endDate = moment(ctrl.startDate).add(2, 'd').toDate();
      ctrl.setEndDate({'seleted':ctrl.endDate});
    }
    ctrl.setStartDate({'seleted':ctrl.startDate});
  }

  ctrl.endDateSelected = (selected) => {
    ctrl.endDate = selected;
    if (moment(ctrl.endDate) < moment(ctrl.startDate)) {
      ctrl.startDate = moment(ctrl.endDate).subtract(2, 'd').toDate();
      ctrl.setStartDate({'seleted':ctrl.startDate});
    }
    ctrl.setEndDate({'seleted':ctrl.endDate});
  }

}
