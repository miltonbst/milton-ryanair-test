export default function AirportSelectorController($scope) {
  'ngInject';

  this.getAirports = () => {
    return this.airportList;
  };

  this.select = (airport) => {
    this.onSelect({selected: airport});
  };

  this.$onInit = () => {
    this.getAirports();
  }

}
