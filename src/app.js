import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngSanitize from 'angular-sanitize';
import Components from './components/components';
import { HomeComponent } from './home/home.component';
import { FlightListComponent } from './components/flight-list/flight-list.component';
import {
  CheapFlightService,
  AirportsService
} from './services';

angular.module('myApp', [
  uiRouter,
  ngSanitize,
  Components
])
.component('homePage', HomeComponent)
.component('flightList', FlightListComponent)
.service('AirportsService', AirportsService)
.service('CheapFlightService', CheapFlightService)
.config(($stateProvider) => {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '',
      template: '<home-page></home-page>'
    })
    .state('home.list', {
      url: '/list?origin&destination&start&end',
      template: '<flight-list></flight-list>'
    });
});
